<?php

declare(strict_types=1);

namespace Paneric\HttpClient;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use JsonException;
use Paneric\Interfaces\HttpClient\HttpClientInterface;
use Psr\Log\LoggerInterface;

class HttpClientAdapter implements HttpClientInterface
{
    public function __construct(
        protected Client $client,
        protected LoggerInterface $logger
    ) {
    }

    /**
     * @throws JsonException
     */
    public function getJsonResponse(
        string $method,
        string $url,
        array $options
    ): array {
        try {
            $response = $this->client->request($method, $url, $options);
            $content = $response->getBody()->getContents();

            $this->logger->info(sprintf(
                '%s %s %s %s',
                'Method: ' . $method,
                'Url: ' . $url,
                'Options: ' . json_encode($options, JSON_THROW_ON_ERROR),
                'Response: ' . $content,
            ));

            $status = $response->getStatusCode();

            if ($response->getHeaderLine('Content-Type') !== 'application/json;charset=utf-8') {
                return [
                    'error' => 'Content type error',
                    'status' => $status
                ];
            }

            $response = json_decode($content, true, 512, JSON_THROW_ON_ERROR);

            return array_merge($response, ['status' => $status]);
        } catch (GuzzleException | JsonException $e) {
            $message = $e->getMessage();
            $this->logger->error(sprintf(
                "%s%s%s%s",
                $e->getFile() . "\n",
                $e->getLine() . "\n",
                $message . "\n",
                $e->getTraceAsString() . "\n"
            ));

            return json_decode(
                substr($message, strpos($message, "{")),
                true,
                512,
                JSON_THROW_ON_ERROR
            );
        }
    }
}
